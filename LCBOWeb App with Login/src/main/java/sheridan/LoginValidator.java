package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		Boolean isValid = false;
		
		if(loginName.length() >= 6) {
			
			if(Pattern.matches("^[a-zA-Z0-9]*$", loginName)) {
				// If first alpha-character is a number
				if(Character.isDigit(loginName.charAt(0))) {
					throw new NumberFormatException("Login Name should not start with a number!");
				} 
				
				else {
					isValid = true;
				}
			}
			
			else {
				throw new NumberFormatException("Login Name should not have special characters!");
			}
		} 
		
		else
			throw new NumberFormatException("Login Name should be atleast 6 or more characters!");
		
		return isValid;
	}
}
