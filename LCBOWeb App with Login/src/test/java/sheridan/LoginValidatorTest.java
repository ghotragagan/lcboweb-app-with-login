package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	// Must have Alpha Characters and Numbers Only and Should not start with a number
	// Atleast 6 alphanumeric characters
	
	// S/N: I could have alot more test cases, but I felt like having multiple
	// boundary in and outs to test additional functionality. 
	
	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid Login" , LoginValidator.isValidLoginName("ghotraga"));
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidLoginException() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("gho"));
	}
	
	@Test
	public void testIsValidLoginBoundaryIn() {
		assertTrue("Invalid Login", LoginValidator.isValidLoginName("ghotra"));
	}
	
	@Test
	public void testIsValidLoginWithAlphaNumbersOnlyBoundaryIn() {
		assertTrue("Invalid Login", LoginValidator.isValidLoginName("gho123"));
	}
	
	@Test
	public void testIsValidLoginWithShouldNotStartWithNumberBoundaryIn() {
		assertTrue("Invalid Login", LoginValidator.isValidLoginName("g1hotr"));
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidLoginBoundaryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("ghot1"));
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidLoginAlphaAndNumbersOnlyBoundaryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("gho12!"));
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidLoginShouldNotStartWithNumberBoundaryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("1hotra"));
	}

	@Test(expected=NumberFormatException.class)
	public void testIsValidLoginShouldBeAtLeast6AlphaNumericCharsBoundaryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("gho12"));
	}
}
